package Weapon;

import Unit.Unit;

public class Weapon {
	protected Unit owner;
    protected final String name;
    public int dmg;
       
    public Weapon(Unit owner, String name, int dmg) {
    	this.owner = owner;
    	this.name = name;
    	this.dmg = (int) (dmg * owner.getState().strength);
    }
        
    public String getName() {
    	return this.name;
    }
    
    public void action(Unit target) {
    	target.takeDmg(this.dmg);
        if ( target.getState().isAlive ) {
            owner.takeDmg(target.getWeapon().dmg / 2);
        }
    }
    
    public String toString() {
    	StringBuffer out = new StringBuffer();
    	out.append(this.getName() + ": DMG(" + this.dmg + ")");
    	return out.toString();
    }
}
