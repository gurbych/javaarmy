package Weapon;

import java.util.Random;

import State.VMPState;
import Unit.Unit;

public class Claws extends Weapon {

	public Claws(Unit owner, String name, int dmg) {
		super(owner, name, dmg);
	}
	
	public Claws(Unit owner) {
		this(owner, "Claws", 15);
	}
	
	public void action(Unit target) {
		super.action(target);

		if ( !target.getState().isAlive ) {
			return;
		}
		
		if ( !target.getState().isUndead && !target.getState().isWerewolf ) {
	        owner.addHP(target.getLimitHP() / 10);
	    }
		
		Random random = new Random();
		int infectionChance = random.nextInt(10);
		
		if ( infectionChance % 2 == 0 && !target.getState().isWerewolf 
				&& !target.getState().isUndead ) {
			target.changeState(new VMPState(target));
			target.changeWeapon(new Claws(target));
		}
	}

}
