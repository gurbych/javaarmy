package Weapon;

import Unit.Unit;

public class Dagger extends Weapon {

	public Dagger(Unit owner, String name, int dmg) {
		super(owner, name, dmg);
	}
	
	public Dagger(Unit owner){
		this(owner, "Dagger", 13);
	}
	
	public void action(Unit target) {
		target.takeDmg(this.dmg);
	}
	
}
