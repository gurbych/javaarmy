package Weapon;

import java.util.Random;
import State.WWFState;
import Unit.Unit;

public class Fangs extends Weapon {

	public Fangs(Unit owner, String name, int dmg) {
		super(owner, name, dmg);
	}
	
	public Fangs(Unit owner) {
		this(owner, "Fangs", 30);
	}
	
	public void action(Unit target) {
		super.action(target);
		
		Random random = new Random();
		int infectionChance = random.nextInt(10);
		
		if ( infectionChance % 2 == 0 && !target.getState().isWerewolf 
				&& !target.getState().isUndead ) {
			target.changeState(new WWFState(target));
			target.changeWeapon(new Fangs(target));
		}
	}

}
