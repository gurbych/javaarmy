package Weapon;

import Unit.Unit;

public class Sword extends Weapon {

	public Sword(Unit owner, String name, int dmg) {
		super(owner, name, dmg);
	}
	
	public Sword(Unit owner) {
		this(owner, "Sword", 20);
	}

}
