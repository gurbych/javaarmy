package State;

import Unit.Unit;

public class State {
	protected int HP;
	protected int limitHP;
	protected int speed;
	protected Unit unit;
	
    public boolean isAlive;
    public boolean isUndead;
    public boolean isWerewolf;
    public double strength;

    public State(Unit unit, int limitHP) {
    	this.unit = unit;
    	this.limitHP = limitHP;
    	this.HP = limitHP;
    }
    
    public void takeDmg(int dmg) {
    	HP -= dmg;
        if ( HP <= 0 ) {
        	this.unit.feedCatchers();
            isAlive = false;
            HP = 0;
        }
    }
   
    public void takeMagicDmg(int magicDmg) {
    	this.takeDmg(magicDmg);
    }
    
    public void addHP(int amount) {
    	HP += amount;
        if ( HP > limitHP ) {
            HP = limitHP;
        }
    }

    public void spendMP(int cost) {}
    
    public void addMP(int amount) {}
    
    public int getHP() {
    	return this.HP;
    }
    
    public int getLimitHP() {
    	return this.limitHP;
    }
    
    public int getMP() {
    	return 0;
    }
    
    public int getLimitMP(){
    	return 0;
    }
}
