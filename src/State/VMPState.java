package State;

import Unit.Unit;

public class VMPState extends State {

	public VMPState(Unit unit) {
		super(unit, 200);
		strength = 2.5;
	    isUndead = true;
	    isAlive = true;
	}

}
