package State;

import Unit.Unit;

public class CSTRState extends State {
	
	private int MP;
	private int limitMP;
	
	public CSTRState(Unit unit, int limitHP, int limitMP) {
		super(unit, limitHP);
	    isAlive = true;
	    isUndead = false;
	    isWerewolf = false;
	    this.strength = 0.5;
		this.MP = limitMP;
		this.limitMP = limitMP;
	}
	
	public int getMP(){
	    return MP;
	}

	public int getLimitMP(){
	    return limitMP;
	}

	public void addMP(int amount){
	    MP += amount;
	    if ( MP > limitMP ) {
	        MP = limitMP;
	    }
	}

	public void spendMP(int cost) {
        MP -= cost;
	}

}
