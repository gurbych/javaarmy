package State;

import Unit.Unit;

public class UNTState extends State {

	public UNTState(Unit unit, int limitHP) {
		super(unit, limitHP);
		strength = 0.5;
	    isAlive = true;
	    isUndead = false;
	    isWerewolf = false;
	}

}
