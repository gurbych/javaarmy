package State;

import Unit.Unit;

public class WWFState extends State {

	public WWFState(Unit target) {
		super(target, target.getLimitHP() * 2);
		this.takeDmg((target.getLimitHP() - target.getHP()) * 2);
		strength = 3;
	    isAlive = true;
	    isUndead = false;
	    isWerewolf = true;

	    System.out.println(target.getName() + " becomes Werewolf");
	}

	public void takeMagicDmg(int magicDmg) {
		this.takeDmg(magicDmg * 2);
	}
	
	

}
