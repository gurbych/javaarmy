package Exceptions;

public class UnitIsDeadException extends Exception {
	
	public String message;

    public UnitIsDeadException() {
        this.message = "Unit is dead";
    }
}
