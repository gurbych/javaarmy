package Observable;

import java.util.HashSet;
import java.util.Set;

public interface SoulCatcher {
	Set<Soul> markedSouls = new HashSet<Soul>();
	
	Set<Soul> getMarkedSouls();
	
	void releaseAllSouls();
	
	public void markTheSoul(Soul soul);

	public void unmarkTheSoul(Soul soul);
}
