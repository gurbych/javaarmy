package Observable;

import java.util.HashSet;
import java.util.Set;
import Unit.Necromancer;

public interface Soul {
	Set<Necromancer> catchers = new HashSet<Necromancer>();

    public void addSoulCatcher(Necromancer catcher);
    	
    public void removeSoulCatcher(Necromancer catcher);
    
    public Set<Necromancer> getCatchers();

    public void feedCatchers();
}
