package Unit;

import java.util.HashMap;
import java.util.Map;

import Exceptions.NotEnaughManaException;
import Exceptions.UnitIsDeadException;
import Spell.Fireball;
import Spell.Heal;
import Spell.Spell;

public class Spellcaster extends Unit {
	
	protected Map<String, Spell> spellbook = new HashMap<String, Spell>();;
	
	protected Map<String, Double> modifiers = new HashMap<String, Double>();
	
	public Spellcaster(String name) {
		super(name);
		
	    modifiers.put("atackMod", 0.5);
	    modifiers.put("hollyMod", 0.5);
	    modifiers.put("wisdoMod", 0.5);

	    this.spellbook.put("Fireball", new Fireball());
	    this.spellbook.put("Heal", new Heal());
	}
	
	protected void checkMP(int cost) throws NotEnaughManaException {
		if (this.state.getMP() < cost) {
			throw new NotEnaughManaException();
		}
	}
	
	protected boolean spellbookContains(Spell newSpell) {
		return spellbook.containsKey(newSpell.getSpellName());
	}
    
	protected double spellingEfficiency(String spellName) {
		return modifiers.get(spellbook.get(spellName).getSpellType());
	}
	
	public Map<String, Spell> getSpellbook() {
		return this.spellbook;
	}
	
	public void addSpell(Spell newSpell) {
		if (this.spellbookContains(newSpell)) {
			System.out.println("This spell is already in spellbook");
			return;
		}
		
		spellbook.put(newSpell.getSpellName(), newSpell);
	}
	

	public void cast(String spellName, Unit target) {
		try {
            isAlive();
        } catch (UnitIsDeadException excDead) {
            System.out.println("Imposable to cast - " + excDead.message);
            return;
        }

		Spell spell = spellbook.get(spellName);	
		double modificator = spellingEfficiency(spellName);
		int realSpellCost = (int) (spell.getCost() / ( modificator * 2));
		
	    try {
	        checkMP(realSpellCost);
	    } catch (NotEnaughManaException error) {
	    	System.out.println("Imposable to cast - " + error.message);
	        return;
	    }
	    
	    state.spendMP(realSpellCost);
	    System.out.println(this.name + " cast " + spellName + " on " + target.getName());
	    spell.action(target, modificator);
	}
	
	public int getMP() {
		return this.state.getMP();
	}
	
	public int getLimitMP() {
		return this.state.getLimitMP();
	}
	
	public String toString() {
        StringBuffer out = new StringBuffer();

        out.append(getName() + (getState().isAlive ? " is Alive" : " is Dead"));
        out.append(" HP(" + getHP() + '/' + getLimitHP() + ')');
        out.append(" MP(" + getMP() + '/' + getLimitMP() + ')');
        out.append(" has weapon: ");
        out.append(getWeapon());

        return out.toString();
    }
}
