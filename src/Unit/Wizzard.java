package Unit;

import State.CSTRState;
import Weapon.Sword;

public class Wizzard extends Spellcaster {

	public Wizzard(String name) {
		super(name);
		this.state = new CSTRState(this, 50, 100);
		this.modifiers.put("atackMod", (double) 2);
		this.weapon = new Sword(this);
	}
	
	public Wizzard() {
		this("Wizzard");
	}
}
