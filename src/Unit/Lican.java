package Unit;

import Exceptions.UnitIsDeadException;
import State.UNTState;
import State.WWFState;
import Weapon.Fangs;
import Weapon.Sword;

public class Lican extends Unit {
	private boolean isHuman;
	
	public Lican(String name) {
		super(name);
		this.state = new UNTState(this, 120);
		this.state.isWerewolf = true;
		state.strength = 1.5;
		this.weapon = new Sword(this);
		this.isHuman = true;
	}
	
	public Lican() {
		this("Lican");
	}
	
	public void transform() {
		try {
            isAlive();
        } catch (UnitIsDeadException excDead){
            System.out.println("Imposible to transform - " + excDead.message);
            return;
        }

	    if ( !this.isHuman ) {
	        int differHP = state.getLimitHP() - state.getHP();
	        UNTState human = new UNTState(this, 120);

	        human.takeDmg(differHP / 2);
	        human.strength = 1.5;
	        changeState(human);
	        System.out.println( getName() + " takes human form");
	        changeWeapon(new Sword(this));
	    } else {
	        changeState(new WWFState(this));
	        changeWeapon(new Fangs(this));
	        this.isHuman = false;
	    }

	}
	
}
