package Unit;

import State.CSTRState;
import Weapon.Sword;

public class Healler extends Spellcaster {

	public Healler(String name) {
		super(name);
		this.state = new CSTRState(this, 60, 100);
		this.modifiers.put("hollyMod", 2.0);
		this.weapon = new Sword(this);
	}
	
	public Healler() {
		this("Healler");
	}

}
