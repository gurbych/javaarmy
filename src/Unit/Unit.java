package Unit;

import java.util.Iterator;
import java.util.Set;

import Observable.Soul;
import State.State;
import Weapon.Weapon;
import Exceptions.UnitIsDeadException;

public class Unit implements Soul {
    
    final protected String name;
    protected State state;
    protected Weapon weapon;

    protected void isAlive() throws UnitIsDeadException {
        if ( !state.isAlive ) {
            throw new UnitIsDeadException();
        }
    }

    public Unit(final String name) {
        this.name = name;
    }

    public void atack(Unit target) {
        try {
            isAlive();
        } catch (UnitIsDeadException excDead){
            System.out.println("Atack is imposable - " + excDead.message);
            excDead.printStackTrace();
            return;
        }
        
        System.out.println();
        System.out.println(getName() + " atacks " + target.getName());
        
    	weapon.action(target);        
    }

    public void addHP(int amount){
        try {
            isAlive();
        } catch (UnitIsDeadException excDead){
        	System.out.println("Healing is imposable - " + excDead.message);
            return;
        }
        
        ***vsdvnjlnlk

        System.out.println(getName() + " healed: +" + amount);
        this.state.addHP(amount);
    }
    
    public void feedCatchers() {
        Iterator<Necromancer> catcher = this.getCatchers().iterator();

        while ( catcher.hasNext() ) {
        	catcher.next().unmarkTheSoul(this);
        	catcher.next().addHP(this.getLimitHP() / 10);
        }
    }
    
    public void addSoulCatcher(Necromancer catcher) {
    	this.getCatchers().add(catcher);
    }
	
    public void removeSoulCatcher(Necromancer catcher) {
    	this.getCatchers().remove(catcher);
    }
    
    public Set<Necromancer> getCatchers() {
    	return Soul.catchers;
    }

    public void changeWeapon(Weapon newWeapon) {
        try {
            isAlive();
        } catch (UnitIsDeadException excDead){
            System.out.println("Can't change weapon - " + excDead.message);
            return;
        }

        this.weapon = newWeapon;
    }

    public void changeState(State newState) {
        try {
            isAlive();
        } catch (UnitIsDeadException excDead) {
            System.out.println("Can't change state - " + excDead.message);
            return;
        }

        this.state = newState;
    }

    public void takeDmg(int dmg) {
        try {
            isAlive();
        } catch (UnitIsDeadException excDead) {
            System.out.println("Imposable to take dmg - " + excDead.message);
            return;
        }

        System.out.println(this.name + " takes " + dmg + " dmg");
        state.takeDmg(dmg);
    }

    public void takeMagicDmg(int magicDmg) {
    	try {
            isAlive();
        } catch (UnitIsDeadException excDead) {
            System.out.println("Imposable to take magic dmg - " + excDead.message);
            return;
        }
    	
    	System.out.println(this.name + " takes " + magicDmg + " magic dmg");
        this.state.takeMagicDmg(magicDmg); 
    }

    public int getHP() {
        return state.getHP();
    }

    public int getLimitHP() {
        return state.getLimitHP();
    }

    public int getMP()  {
        return 0;
    }

    public int getLimitMP() {
        return 0;
    }

    public String getName() {
        return name;
    }

    public State getState()  {
        return state;
    }

    public Weapon getWeapon()  {
        return weapon;
    }

    public String toString() {
        StringBuffer out = new StringBuffer();

        out.append(getName() + (getState().isAlive ? " is Alive" : " is Dead"));
        out.append(" HP(" + getHP() + '/' + getLimitHP() + ')');
        out.append(" has weapon: ");
        out.append(getWeapon());

        return out.toString();
    }
}

