package Unit;

import State.VMPState;
import Weapon.Claws;

public class Vampire extends Unit {

	public Vampire(String name) {
		super(name);
		this.state = new VMPState(this);
		this.weapon = new Claws(this);
	}

	public Vampire() {
		this("Vampire");
	}
}
