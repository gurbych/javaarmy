package Unit;

import State.UNTState;
import Weapon.Sword;

public class Berseker extends Unit {

	public Berseker(String name) {
		super(name);
		state = new UNTState(this, 78);
	    state.strength = 1.5;
	    this.weapon = new Sword(this);
	}
	
	public Berseker() {
		this("Berseker");
	}
	
	public void takeMagicDmg(int dmg) {
		System.out.println(this.name + ": AHAHAH useless triks");
	}

}
