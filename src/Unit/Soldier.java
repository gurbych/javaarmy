package Unit;

import State.UNTState;
import Weapon.Sword;

public class Soldier extends Unit {

	public Soldier(String name) {
		super(name);
		state = new UNTState(this, 100);
	    state.strength = 1.2;
	    this.weapon = new Sword(this);
	}

	public Soldier() {
		this("Soldier");
	}
	
}
