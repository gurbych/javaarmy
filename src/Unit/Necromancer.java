package Unit;

import java.util.Iterator;
import java.util.Set;
import Observable.Soul;
import Observable.SoulCatcher;
import State.CSTRState;
import Weapon.Sword;

public class Necromancer extends Spellcaster implements SoulCatcher{

	public Necromancer(String name) {
		super(name);
		this.state = new CSTRState(this, 50, 110);
		this.modifiers.put("atackMod", 1.5);
		this.modifiers.put("wisdom", 1.0);
		this.weapon = new Sword(this);
	}
	
	public void cast(String spellName, Unit target) {
		super.cast(spellName, target);
		
		if ( target.getState().isUndead ) {
			return;
		}
		
		markTheSoul(target);
	}
	
	public void takeDmg(int dmg) {
        super.takeDmg(dmg);
        
        if ( !this.state.isAlive ) {
        	releaseAllSouls();	
        }
    }

    public void takeMagicDmg(int magicDmg) {
    	super.takeMagicDmg(magicDmg);
        
        if ( !this.state.isAlive ) {
        	releaseAllSouls();	
        }
    }
	
	public Necromancer() {
		this("Necromancer");
	}
	
	public Set<Soul> getMarkedSouls() {
		return SoulCatcher.markedSouls;
	}
	
	public void markTheSoul(Soul soul) {
		this.getMarkedSouls().add(soul);
		soul.addSoulCatcher(this);
	}

	public void unmarkTheSoul(Soul soul) {
		this.getMarkedSouls().remove(soul);
		soul.removeSoulCatcher(this);
	}
	
	public void releaseAllSouls() {
		Iterator<Soul> soul = this.getMarkedSouls().iterator();

		while ( soul.hasNext() ) {
			((Soul) soul).removeSoulCatcher(this);
        }
	}
}
