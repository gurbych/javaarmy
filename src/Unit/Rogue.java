package Unit;

import State.UNTState;
import Weapon.Dagger;

public class Rogue extends Unit {

	public Rogue(String name) {
		super(name);
		state = new UNTState(this, 80);
	    state.strength = 1;
	    this.weapon = new Dagger(this);
	}
	
	public Rogue() {
		this("Rogue");
	}
	
}
