package Spell;

import Unit.Unit;

public class Heal extends Spell {

	public Heal(String name, int manaCost, int basicPower) {
		super(name, manaCost, basicPower);
		this.type = "holy";
	}
	
	public Heal() {
		this("Heal", 80, 20);
	}
	
	public void action(Unit target, double casterMod) {
		if ( !target.getState().isUndead ) {
	        target.addHP((int) (basicPower * casterMod));
	    } else {
	        System.out.println("Target cant be healed");
	    }
	}

}
