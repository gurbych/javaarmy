package Spell;

import Unit.Unit;

public class Fireball extends Spell {
	
	public Fireball(String name, int manaCost, int basicPower) {
		super(name, manaCost, basicPower);
		this.type = "atackMod";
	}
	
	public Fireball() {
		this("Fireball", 80, 20);
	}
	
	public void action(Unit target, double casterMod) {
		target.takeMagicDmg((int) (basicPower * casterMod));
	}
}
