package Spell;

import Unit.Unit;

public class Spell {
	final protected String name;
	final protected int manaCost;
	final protected int basicPower;
	protected String type;
    
    public Spell(String name, int manaCost, int basicPower) {
    	this.name = name;
    	this.manaCost = manaCost;
    	this.basicPower = basicPower;
    }
    
    public void action(Unit target, double casterMod) {}

    public int getCost() {
    	return manaCost;
    }
    public int getBasicPower() {
    	return basicPower;
    }

    public String getSpellType() {
    	return type;
    }
    
    public String getSpellName() {
    	return name;
    }
    
    public String toString() {
    	StringBuffer out = new StringBuffer();
    	
    	out.append(this.getSpellName() + " (");
        out.append("c:" + this.getCost());
        out.append("; p:" + this.getBasicPower());
        out.append(")");
    	
		return out.toString();
    }
    
}
